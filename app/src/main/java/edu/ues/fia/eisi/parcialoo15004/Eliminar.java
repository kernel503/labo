package edu.ues.fia.eisi.parcialoo15004;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.TextView;

import org.w3c.dom.Text;


public class Eliminar extends AppCompatActivity {
    ControlBD BDhelper;
    TextView editCentral;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_eliminar);
        BDhelper=new ControlBD(this);
        editCentral = findViewById(R.id.txtCentral);
        editCentral.setText(BDhelper.central());
    }
}
