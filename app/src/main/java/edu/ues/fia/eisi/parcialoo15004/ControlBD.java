package edu.ues.fia.eisi.parcialoo15004;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;

public class ControlBD {

    private final Context context;
    private DatabaseHelper DBHelper;
    private SQLiteDatabase db;
    private static final String Tag = "Mensajes";

    public ControlBD(Context ctx) {
        this.context = ctx;
        DBHelper = new DatabaseHelper(context);
    }

    private static class DatabaseHelper extends SQLiteOpenHelper {
        private static final String BASE_DATOS = "oo15004P2.s3db";
        private static final int VERSION = 1;

        public DatabaseHelper(Context context) {
            super(context, BASE_DATOS, null, VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            try{
                db.execSQL("CREATE TABLE depto (coddepto INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, nomdepto VARCHAR(30) NOT NULL, zona VARCHAR(12) NOT NULL);");
                db.execSQL("CREATE TABLE proveedor (codprov INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, nombre VARCHAR(30) NOT NULL,apellido VARCHAR(30) NOT NULL,comprasz_central INTEGER, comprasz_oriental INTEGER);");
                db.execSQL("CREATE TABLE compra (numfactura VARCHAR(5), codprov INTEGER NOT NULL, coddepto INTEGER NOT NULL, monto FLOAT NOT NULL, FOREIGN KEY(coddepto) REFERENCES depto, PRIMARY KEY(numfactura),FOREIGN KEY(codprov) REFERENCES proveedor);");
                Log.i(Tag, "SE CREO EXITOSAMENTE");
            }catch(SQLException e){
                e.printStackTrace();
                Log.i(Tag, "NO SE CREO");
            }
        }
        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            // TODO Auto-generated method stub
        }

    }

    public void abrir() throws SQLException{
        db = DBHelper.getWritableDatabase();
        return;
    }

    public void cerrar(){
        DBHelper.close();
    }

    public String llenarBase(){
        String valor ="";
        abrir();
        try {
            db.execSQL("DROP TABLE proveedor;");
            db.execSQL("DROP TABLE depto;");
            db.execSQL("DROP TABLE compra;");
            db.execSQL("CREATE TABLE depto (coddepto INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, nomdepto VARCHAR(30) NOT NULL, zona VARCHAR(12) NOT NULL);");
            db.execSQL("CREATE TABLE proveedor (codprov INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, nombre VARCHAR(30) NOT NULL,apellido VARCHAR(30) NOT NULL,comprasz_central INTEGER, comprasz_oriental INTEGER);");
            db.execSQL("CREATE TABLE compra (numfactura VARCHAR(5), codprov INTEGER NOT NULL, coddepto INTEGER NOT NULL, monto FLOAT NOT NULL, FOREIGN KEY(coddepto) REFERENCES depto, PRIMARY KEY(numfactura),FOREIGN KEY(codprov) REFERENCES proveedor);");
            db.execSQL("INSERT INTO proveedor (codprov,nombre,apellido,comprasz_central,comprasz_oriental) VALUES (1,'Raúl','Ramos',1,1);");
            db.execSQL("INSERT INTO proveedor (codprov,nombre,apellido,comprasz_central,comprasz_oriental) VALUES (2,'Ramon','Perez',2,2);");
            db.execSQL("INSERT INTO depto (coddepto,nomdepto,zona) VALUES (1,'San Salvador','Central');");
            db.execSQL("INSERT INTO depto (coddepto,nomdepto,zona) VALUES (2,'Santa Ana','Occidente');");
            db.execSQL("INSERT INTO depto (coddepto,nomdepto,zona) VALUES (3,'Sonsonate','Occidente');");
            db.execSQL("INSERT INTO depto (coddepto,nomdepto,zona) VALUES (4,'Ahuachapan','Occidente');");
            db.execSQL("INSERT INTO depto (coddepto,nomdepto,zona) VALUES (5,'Chalatenango','Central');");
            db.execSQL("INSERT INTO compra (numfactura,codprov,coddepto,monto) VALUES ('abc11',1,2,10.0);");
            db.execSQL("INSERT INTO compra (numfactura,codprov,coddepto,monto) VALUES ('abc12',1,5,20.0);");
            db.execSQL("INSERT INTO compra (numfactura,codprov,coddepto,monto) VALUES ('abc13',2,1,100.0);");
            db.execSQL("INSERT INTO compra (numfactura,codprov,coddepto,monto) VALUES ('abc14',2,2,200.0);");
            db.execSQL("INSERT INTO compra (numfactura,codprov,coddepto,monto) VALUES ('abc15',2,3,300.0);");
            db.execSQL("INSERT INTO compra (numfactura,codprov,coddepto,monto) VALUES ('abc16',2,1,1500.0);");
            db.execSQL("DROP TRIGGER IF EXISTS decremento;");
            db.execSQL("CREATE TRIGGER decremento AFTER DELETE ON compra FOR EACH ROW WHEN old.coddepto = 1 OR old.coddepto = 5 BEGIN UPDATE proveedor SET comprasz_central = comprasz_central-1 WHERE codprov = old.codprov;END;");
            valor = "Se Guardo Correctamente";
        }catch (SQLException ex){
            valor = "Ocurrio un problema";
            Log.i(Tag, "Revisar sintaxis ");
        }
        cerrar();
        return valor;
    }

    public ArrayList listaCompra(){
        abrir();
        ArrayList<compra> lista = new ArrayList<>();
        try{
            Cursor c= db.rawQuery("SELECT c.numfactura, d.nomdepto, d.zona," +
                    " p.nombre, p.apellido, c.monto, d.coddepto, p.codprov FROM compra AS c " +
                    "INNER JOIN depto AS d USING (coddepto) " +
                    "INNER JOIN proveedor AS p USING (codprov)",null);
            if(c.moveToFirst()){
                do{
                    compra u = new compra();
                    u.setNumfactura(c.getString(0));
                    u.setNomdepto("Depto: "+c.getString(1));
                    u.setZona(c.getString(2));
                    u.setNombre(c.getString(3)+" "+c.getString(4));
                    u.setMonto(c.getFloat(5));
                    u.setCoddepto(c.getInt(6));
                    u.setCodprov(c.getInt(7));
                    lista.add(u);
                }while (c.moveToNext());
            }
        }catch (SQLException ex){ }
        cerrar();
        return lista;
    }

    public String actualizarCompra(String a,String b,String c, String cod){
        abrir();
        String s = "";
        String[] args = new String[4];
        args[0]=a;
        args[1]=b;
        args[2]=c;
        args[3]=cod;
        try{
            db.execSQL("UPDATE compra SET codprov = ?, coddepto = ?, monto = ? WHERE numFactura = ?",args);
            s="Actualizado";
        }catch (SQLException ex){
            s="Error";
        }
        cerrar();
        return  s;
    }

    public boolean existeDept(String d){
        abrir();
        boolean b = false;
        String[] args = new String[1];
        args[0]=d;
        try{
            Cursor c= db.rawQuery("SELECT * FROM depto WHERE coddepto = ?",args);
            if(c.moveToFirst()){
                b = true;
            }else{
                b = false;
            }

        }catch (SQLException ex){ }
        cerrar();
        return  b;
    }

    public boolean existeProv(String id){
        abrir();
        boolean b = false;
        String[] args = new String[1];
        args[0]=id;
        try{
            Cursor c= db.rawQuery("SELECT * FROM proveedor WHERE codprov = ?",args);
            if(c.moveToFirst()){
                b = true;
            }else{
                b = false;
            }
        }catch (SQLException ex){ }
        cerrar();
        return  b;
    }

    public double promedio(String d){
        abrir();
        double f = 0.0;
        String[] args = new String[1];
        args[0]=""+d;
        try{
            Cursor c= db.rawQuery("SELECT avg(monto) FROM compra WHERE codprov = 2 AND monto < ?",args);
            if(c.moveToFirst()){
                f = c.getDouble(0);
            }
        }catch (SQLException ex){ }
        cerrar();
        return  f;
    }

    public String central(){
        abrir();
        String s ="";
        try{
            Cursor c= db.rawQuery("SELECT SUM(comprasz_central) FROM proveedor ",null);
            if(c.moveToFirst()){
                do{
                    s = "Cantidad de ventas zona central: "+c.getString(0);
                }while (c.moveToNext());
            }
        }catch (SQLException ex){ }
        cerrar();
        return  s;
    }

    public String Eliminar(String numfactura){
        String s ="";
        abrir();
        String[] args = new String[1];
        args[0]=numfactura;
        try{
            db.execSQL("DELETE FROM compra WHERE numfactura = ?;",args);
            s="Se elimino";
            } catch (SQLException ex){
            s="No se elimino";
        }
        cerrar();
        return  s;
    }




}
