package edu.ues.fia.eisi.parcialoo15004;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class actualizar extends AppCompatActivity {


    ControlBD BDhelper;
    Button actualizar, eliminar;
    ListView listView;
    ArrayList<compra> lista;
    private ArrayList<String> stringArrayList;
    private ArrayAdapter<String> stringArrayAdapter;
    String idPrincipal = "";


    Float monto;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actualizar);


        BDhelper=new ControlBD(this);
        actualizar = findViewById(R.id.btnActualizar);
        eliminar= findViewById(R.id.btnEliminar);
        listView = findViewById(R.id.lista);
        actualizar.setEnabled(false);
        eliminar.setEnabled(false);
        llenarTabla();

        // CLICK LISTVIEW
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int index, long l) {
                Object clickItemObj = adapterView.getAdapter().getItem(index);

                //INICIO

                idPrincipal = lista.get(index).getNumfactura();
                Toast.makeText(getBaseContext(),idPrincipal,Toast.LENGTH_SHORT).show();
                eliminar.setEnabled(true);
                String zona = lista.get(index).getZona();
                String nombre = lista.get(index).getNombre();
                monto = lista.get(index).getMonto();
                if (zona.equals("Occidente") && nombre.equals("Raúl Ramos")){
                    Toast.makeText(getBaseContext(),"INVALIDO",Toast.LENGTH_SHORT).show();
                    actualizar.setEnabled(false);
                }else{
                    actualizar.setEnabled(true);
                }
                //FIN

            }
        });




    }

    private void llenarTabla(){
        lista = BDhelper.listaCompra();
        stringArrayList= new ArrayList<>();
        if (!lista.isEmpty()){
            for (compra a : lista){
                stringArrayList.add(a.getNomdepto()+"\nZona: "+a.getZona()+"\nNombre: "+a.getNombre()
                        +"\n"+"Monto: "+a.getMonto()+" ("+a.getNumfactura()+")");
            }
        }else {
            stringArrayList.add("Lista Vacia");
        }
        stringArrayAdapter =new ArrayAdapter<>(this, android.R.layout.simple_list_item_activated_1,stringArrayList);
        listView.setAdapter(stringArrayAdapter);
    }







    public void actualizar(View v){
        Comprobar();
    }

    public void Comprobar(){

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        Context context = this;
        LinearLayout layout = new LinearLayout(context);
        layout.setOrientation(LinearLayout.VERTICAL);

        final TextView txt1 = new TextView(context);
        txt1.setText("Num Factura: " +idPrincipal);
        layout.addView(txt1);

        final TextView txt2 = new TextView(context);
        txt2.setText("Cod Proveedor");
        layout.addView(txt2);

        final EditText edit1 = new EditText(context);
        edit1.setInputType(InputType.TYPE_CLASS_NUMBER);
        layout.addView(edit1);

        final TextView txt3 = new TextView(context);
        txt3.setText("Cod Departamento");
        layout.addView(txt3);

        final EditText edit2 = new EditText(context);
        edit2.setInputType(InputType.TYPE_CLASS_NUMBER);
        layout.addView(edit2);

        final TextView txt4 = new TextView(context);
        txt4.setText("Monto:");
        layout.addView(txt4);

        final EditText edit3 = new EditText(context);
        edit3.setInputType(InputType.TYPE_CLASS_NUMBER);
        layout.addView(edit3);

        builder.setView(layout);
        builder.setPositiveButton("Aceptar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String a,b,c,d;

                a=edit1.getText().toString();
                b=edit2.getText().toString();
                c=edit3.getText().toString();


                if (!BDhelper.existeProv(a)){
                    Comprobar();
                    Toast.makeText(getBaseContext(),"No existe Proveedor",Toast.LENGTH_SHORT).show();
                }

                if (!BDhelper.existeDept(b)){
                    Comprobar();
                    Toast.makeText(getBaseContext(),"No existe Departamento",Toast.LENGTH_SHORT).show();
                }


                //if (BDhelper.existeProv(a) && BDhelper.existeDept(b)){
                    //Toast.makeText(getBaseContext(),BDhelper.actualizarCompra(a,b,c,numFactura),Toast.LENGTH_SHORT).show();
                    //llenarTabla();
                //}
            }
        });
        builder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();
    }

    public void eliminar(View v){
        Toast.makeText(getBaseContext(), BDhelper.Eliminar(idPrincipal),Toast.LENGTH_SHORT).show();
        llenarTabla();
    }

    

}
