package edu.ues.fia.eisi.parcialoo15004;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    ControlBD BDhelper;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        BDhelper=new ControlBD(this);
    }

    public void llenarBase (View v){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(true);
        builder.setTitle("Drop Table");
        builder.setMessage("Restaurar valores de las tablas");
        builder.setPositiveButton("Confirmar",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Toast.makeText(getBaseContext(),BDhelper.llenarBase(),Toast.LENGTH_SHORT).show();
                    }
                });
        builder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
            }
        });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    public void actualizar(View v){
        Intent i = new Intent(getBaseContext(), actualizar.class);
        startActivity(i);
    }

    public void promedio(View v){
        Intent i = new Intent(getBaseContext(), promedio.class);
        startActivity(i);
    }

    public void eliminar(View v){
        Intent i = new Intent(getBaseContext(), Eliminar.class);
        startActivity(i);
    }

}
