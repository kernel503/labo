package edu.ues.fia.eisi.parcialoo15004;

public class compra {


    String numfactura, nomdepto, zona, nombre;
    Float monto;
    int coddepto, codprov;

    public int getCoddepto() {
        return coddepto;
    }

    public void setCoddepto(int coddepto) {
        this.coddepto = coddepto;
    }

    public int getCodprov() {
        return codprov;
    }

    public void setCodprov(int codprov) {
        this.codprov = codprov;
    }

    public compra() {
    }

    public String getNumfactura() {
        return numfactura;
    }

    public void setNumfactura(String numfactura) {
        this.numfactura = numfactura;
    }

    public String getNomdepto() {
        return nomdepto;
    }

    public void setNomdepto(String nomdepto) {
        this.nomdepto = nomdepto;
    }

    public String getZona() {
        return zona;
    }

    public void setZona(String zona) {
        this.zona = zona;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Float getMonto() {
        return monto;
    }

    public void setMonto(Float monto) {
        this.monto = monto;
    }
}
