package edu.ues.fia.eisi.parcialoo15004;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class promedio extends AppCompatActivity {
    ControlBD BDhelper;
    EditText min;
    TextView pro;
    String valor ="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_promedio);
        BDhelper=new ControlBD(this);

    }

    public void mostrarPromedio(View v){
        min = findViewById(R.id.pedit);
        pro = findViewById(R.id.ptxt3);
        valor = min.getText().toString();
        if (valor.isEmpty()){
            min.setError("Campo Necesario");
        }else{
            pro.setText("Promedio: "+BDhelper.promedio(valor));
            min.setText("");
        }

    }
}
